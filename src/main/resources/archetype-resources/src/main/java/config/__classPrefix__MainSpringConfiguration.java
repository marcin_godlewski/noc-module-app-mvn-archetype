#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.config;

import ${groupId}.commons.spring.marker.MainSpringConfiguration;
import ${groupId}.commons.web.server.exception.GenericExceptionMapper;
import ${groupId}.commons.web.server.exception.WebApplicationExceptionMapper;
import ${groupId}.commons.web.server.spring.ServletContextInitializerHelper;
import ${package}.endpoint.api.HealthCheckResource;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import static ${groupId}.commons.web.server.security.WebApplication.AUTH0_BACKEND;

@Configuration
public class ${classPrefix}MainSpringConfiguration implements MainSpringConfiguration {

    @Bean
    HealthCheckResource healthCheckResource() {
        return new HealthCheckResource();
    }

    @Bean
    GenericExceptionMapper genericExceptionMapper() {
        return new GenericExceptionMapper();
    }

    @Bean
    ServletContextInitializer servletContextInitializer() {
        return ServletContextInitializerHelper.createServletContextInitializer(AUTH0_BACKEND);
    }

    @Bean
    WebApplicationExceptionMapper webApplicationExceptionMapper() {
        return new WebApplicationExceptionMapper();
    }
}