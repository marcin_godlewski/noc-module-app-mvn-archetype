#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.config;

import ${groupId}.modelstore.PartnerModelStoreMainSpringConfiguration;
import ${groupId}.noc.NocMainSpringConfiguration;
import ${groupId}.security.IvanSecurityMainAutoConfiguration;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

@Configuration
@Import({
        IvanSecurityMainAutoConfiguration.class,
        NocMainSpringConfiguration.class,
        PartnerModelStoreMainSpringConfiguration.class,
})
public class SecurityConfiguration {
}