#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

import ${groupId}.commons.web.server.spring.IvanWebSpringBootApplication;
import ${package}.config.${classPrefix}MainSpringConfiguration;
import org.springframework.boot.WebApplicationType;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.Import;

import static org.springframework.boot.Banner.Mode.OFF;

@IvanWebSpringBootApplication
@Import({
        ${classPrefix}MainSpringConfiguration.class
})
public class ${classPrefix}SpringBootApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(${classPrefix}SpringBootApplication.class)
                .bannerMode(OFF)
                .web(WebApplicationType.SERVLET)
                .run(args);
    }
}
