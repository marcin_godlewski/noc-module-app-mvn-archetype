#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.endpoint.api;

import ${package}.endpoint.api.WebContext;
import ${groupId}.commons.web.server.security.PermitAnonymous;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

/**
 * Endpoint probed by platform to determine availability.  The point of this probe is to act as a client to see if it can reach the paths
 * defined in the ingress object.
 */
@Path("/")
public class HealthCheckResource {

    @GET
    @Path(WebContext.ROOT + "/")
    @PermitAnonymous
    public Response ping() {
        return Response.ok("OK")
                .build();
    }
}