package ${package}.endpoint;

import ${package}.endpoint.api.WebContext;

public interface ResourceVersion {

    String VERSION_1 = WebContext.ROOT + "/v1";
}