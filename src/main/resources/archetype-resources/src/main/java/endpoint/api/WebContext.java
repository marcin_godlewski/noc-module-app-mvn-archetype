#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.endpoint.api;

public interface WebContext {

    String ROOT = "/${artifactId}";
}
