#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package}.config;

import ${groupId}.commons.spring.marker.TestSpringConfiguration;
import org.springframework.context.annotation.Configuration;

@Configuration
class ${classPrefix}SpringTestConfiguration extends ${classPrefix}MainSpringConfiguration implements TestSpringConfiguration {
}
